// Copyright (C) 2015 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file mainwindow.h
//  @author Luis Gonzalez <lc.gonzalez23@gmail.com>
//  @version 1.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of NatNet Packet Client.
//
//                          License Agreement
//                     For the NatNet Packet Client
//
//    NatNet Packet Client is a program used to directly communicate to
//    a NatNet server, without using NatNet's API.
//    Copyright (C) 2015 Luis Carlos Gonzalez Garcia
//
//    NatNet Packet Client is free software: you can redistribute it and/or
//    modify it under the terms of the GNU General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    NatNet Packet Client is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "natnet.h"
#include "q_debugstream.h"

#include <QMainWindow>
#include <memory>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_connectButton_clicked();
    void on_sendCommandButton_clicked();

    void on_disconnectButton_clicked();

private:
    Ui::MainWindow *ui;
    std::auto_ptr<Q_DebugStream> Q_DebugStream_;
    NatNet Natnet;
    void populate_network_IFace();
};

#endif // MAINWINDOW_H
