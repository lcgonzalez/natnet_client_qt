// Copyright (C) 2015 Luis Carlos Gonzalez Garcia.
/**
//  @file q_debugstream.cpp
//  @author Luis Gonzalez <lc.gonzalez23@gmail.com>
//  @version 1.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of NatNet Packet Client.
//
//                          License Agreement
//                     For the NatNet Packet Client
//
//    NatNet Packet Client is a program used to directly communicate to
//    a NatNet server, without using NatNet's API.
//    Copyright (C) 2015 Luis Carlos Gonzalez Garcia
//
//    NatNet Packet Client is free software: you can redistribute it and/or
//    modify it under the terms of the GNU General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    NatNet Packet Client is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//*/

#include "q_debugstream.h"

Q_DebugStream::Q_DebugStream(std::ostream &stream, QTextEdit *pTextEdit) : mStream_(stream)
{
    pLogWindow_ = pTextEdit;
    mOldBuf_ = stream.rdbuf();
    stream.rdbuf(this);
}

Q_DebugStream::~Q_DebugStream()
{
    mStream_.rdbuf(mOldBuf_);
}

void Q_DebugStream::myQDebugMessageHandler(QtMsgType msgType, const QMessageLogContext &msgLogContext, const QString &msg)
{
    std::cout << msg.toStdString().c_str();
}

void Q_DebugStream::registerQDebugMessageHandler()
{
    qInstallMessageHandler(myQDebugMessageHandler);
}

std::basic_streambuf<char>::int_type Q_DebugStream::overflow(std::basic_streambuf<char>::int_type v)
{
    if (v == '\n')
    {
        pLogWindow_->append("");
    }
    return v;
}

std::streamsize Q_DebugStream::xsputn(const char *pNotC_str, std::streamsize n)
{
    // Note, if the string array "pNotC_str" represents a number, it is not a c_string,
    // so it doesn't end with the null character.
    // For this reason, the following code, that turns the array into a c_string before
    // turning the array into a Qstring, was added.

    char *pC_str= new char[n+1];
    for(int i=0; i<n; i++){
        pC_str[i] = pNotC_str[i];
    }
    pC_str[n] = 0;   // Terminating null character of c_string

    QString str = QString::fromUtf8(pC_str);

    // Free memory after turning c_str to QString
    if(pC_str)
        delete[] pC_str;

    if(str.contains("\n")){
        QStringList strSplitted = str.split("\n");

        pLogWindow_->moveCursor (QTextCursor::End);
        pLogWindow_->insertPlainText (strSplitted.at(0)); //Index 0 is still on the same old line

        for(int i = 1; i < strSplitted.size(); i++){
            pLogWindow_->append(strSplitted.at(i));
        }
    }else{
        pLogWindow_->moveCursor (QTextCursor::End);
        pLogWindow_->insertPlainText (str);
    }
    return n;
}
