// Copyright (C) 2015 Luis Carlos Gonzalez Garcia.
/**
//  @file q_debugstream.h
//  @author Luis Gonzalez <lc.gonzalez23@gmail.com>
//  @version 1.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of NatNet Packet Client.
//
//                          License Agreement
//                     For the NatNet Packet Client
//
//    NatNet Packet Client is a program used to directly communicate to
//    a NatNet server, without using NatNet's API.
//    Copyright (C) 2015 Luis Carlos Gonzalez Garcia
//
//    NatNet Packet Client is free software: you can redistribute it and/or
//    modify it under the terms of the GNU General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    NatNet Packet Client is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//*/

#ifndef Q_DEBUGSTREAM_H
#define Q_DEBUGSTREAM_H

#include <iostream>
#include <streambuf>
#include <string>

#include <QTextEdit>

class Q_DebugStream : public std::basic_streambuf<char>
{
private:
    std::ostream &mStream_;     // Stream (cout)
    std::streambuf *mOldBuf_;   // Old stream buffer associated with stream (cout)
    QTextEdit* pLogWindow_;

    Q_DebugStream();    // Doesn't allow automatic constuctor
    static void myQDebugMessageHandler(QtMsgType msgType, const QMessageLogContext &msgLogContext, const QString &msg);

public:
    Q_DebugStream(std::ostream &stream, QTextEdit* pTextEdit); // Redirect Console output to QTextEdit
    virtual ~Q_DebugStream();   // Redirect QTextEdit to Console
    static void registerQDebugMessageHandler(); // Redirect qDebug() output to QTextEdit

protected:
    // This is called when a std::endl has been inserted into the stream
    virtual int_type overflow(int_type v);
    // This method is called whenever a stream is processed
    virtual std::streamsize xsputn(const char *pNotC_str, std::streamsize n);
};

#endif // Q_DEBUGSTREAM_H
