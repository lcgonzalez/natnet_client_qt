// Copyright (C) 2015 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file natnet.cpp
//  @author Luis Gonzalez <lc.gonzalez23@gmail.com>
//  @version 1.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of NatNet Packet Client.
//
//                          License Agreement
//                     For the NatNet Packet Client
//
//    NatNet Packet Client is a program used to directly communicate to
//    a NatNet server, without using NatNet's API.
//    Copyright (C) 2015 Luis Carlos Gonzalez Garcia
//
//    NatNet Packet Client is free software: you can redistribute it and/or
//    modify it under the terms of the GNU General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    NatNet Packet Client is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//*/


#include "natnet.h"

#include <iostream>
#include <sstream>  //stringstream
#include <iomanip>  //setw, setfill
#include <cstring>
#include <memory>
#include <QNetworkInterface>

NatNet::NatNet(QObject *parent) :
    QObject(parent),
    NatNetVersion_(),
    state_(NOT_CONNECTED),
    appID("[PacketClient]"),
    notFirstTimeLatency_(false)
{
    // This signal is emitted once every time new data is available for reading
    // from the device.
    // Read commandSocket_ data (unicast and commands) in the commandReading() slot
    connect(&commandSocket_, SIGNAL(readyRead()),
            this, SLOT(commandReading()));

    populateBuggyServers();
}

NatNet::~NatNet()
{
    disconnect();
}

int NatNet::start(QString serverAddres, QString clientAddress)
{
    if(state_ == NOT_CONNECTED) {
        serverAddress_.setAddress(serverAddres);
        myAddress_.setAddress(clientAddress);
        commandPort_ = 1510;
        dataPort_ = 1511;
        multicastAddress_.setAddress("239.255.42.99"); // IANA, local network

        state_ = CONNECTING;
        if(serverAddres.isEmpty()){ // Attempt autoconnect to the server using multicasting
            autoConnectToServer();
        } else {        // Connect to the server using the address provided
            createSockets();
            initCommunication();
        }
    } else if( state_ == CONNECTING) {
        std::cout << appID << " attempting handshake with server: " << serverAddress_.toString().toStdString() << std::endl;
        initCommunication();
    } else if( state_ == CONNECTED){
        std::cout << appID << " already connected to server: " << serverAddress_.toString().toStdString() << std::endl;
    }
}

void NatNet::disconnect()
{
    commandSocket_.close();
    dataSocket_.close();
    state_ = NOT_CONNECTED;
}

void NatNet::commandReading()
{

    NatNet::sPacket packetIn;
    QHostAddress theirAddress;
    commandSocket_.readDatagram((char *)&packetIn, sizeof(NatNet::sPacket), &theirAddress);
    // debug - print message
    std::cout << "[Client ]Received command from: " << theirAddress.toString().toStdString()
              << ": Command=" << packetIn.iMessage << ", nDataBytes=" <<
                 packetIn.nDataBytes <<std::endl;

    // handle command
    switch (packetIn.iMessage)
    {
    case NAT_MODELDEF:
        unpack((char*)&packetIn);
        break;
    case NAT_FRAMEOFDATA:
        unpack((char*)&packetIn);
        break;
    case NAT_PINGRESPONSE:
        std::cout << "[Client] received 'NAT_PINGRESPONSE'" << std::endl;
        setVersions(packetIn);
        break;
    case NAT_RESPONSE:
        {
            char* szResponse = (char *)packetIn.Data.cData;
            std::cout << "Response : " << szResponse;
            break;
        }
    case NAT_UNRECOGNIZED_REQUEST:
        std::cout << "[Client] received 'unrecognized request'" << std::endl;
        break;
    case NAT_MESSAGESTRING:
        std::cout << "[Client] Received message: " << packetIn.Data.szData << std::endl;
        break;
    }
}

void NatNet::dataReading()
{
    char  szData[20000];
    QHostAddress theirAddress;

    dataSocket_.readDatagram(szData, sizeof(szData), &theirAddress);
    unpack(szData);
}

void NatNet::waitForServer()
{
    char  szData[20000];
    QHostAddress theirAddress;

    dataSocket_.readDatagram(szData, sizeof(szData), &theirAddress);
    if(!theirAddress.isNull()){
        // Disconnect this slot in order to wait data streaming (milticat) on dataReading slot
        QObject::disconnect(&dataSocket_, SIGNAL(readyRead()),
                this, SLOT(waitForServer()));

        std::cout << "Auto server address: " << theirAddress.toString().toStdString() << std::endl;
        serverAddress_ = theirAddress;
        initCommunication();
    }
}

bool NatNet::decodeTimecode(unsigned int inTimecode, unsigned int inTimecodeSubframe, int* hour, int* minute, int* second, int* frame, int* subframe)
{
    bool bValid = true;

    *hour = (inTimecode>>24)&255;
    *minute = (inTimecode>>16)&255;
    *second = (inTimecode>>8)&255;
    *frame = inTimecode&255;
    *subframe = inTimecodeSubframe;

    return bValid;
}

bool NatNet::timecodeStringify(unsigned int inTimecode, unsigned int inTimecodeSubframe, std::string &str)
{
    bool bValid;

    // Check for SMPTE timecode validity (not zero)
    if( !(inTimecode | inTimecodeSubframe) )
        return false;

    int hour, minute, second, frame, subframe;
    bValid = decodeTimecode(inTimecode, inTimecodeSubframe, &hour, &minute, &second, &frame, &subframe);

    std::stringstream strStream;
    strStream << std::setfill('0') <<
                 std::setw(2) << hour << ":"  <<
                 std::setw(2) << minute << ":" <<
                 std::setw(2) << second << ":" <<
                 std::setw(2) << frame << "." << subframe;
    str = strStream.str();

    return bValid;
}

bool NatNet::createSockets()
{
    if(commandSocket_.state() == QUdpSocket::UnconnectedState) {
        commandSocket_.bind(myAddress_, 0, QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
        if(commandSocket_.state() != QUdpSocket::BoundState) {
            std::cout << "[PacketClient] command socket bind failed! (error: "
                      << commandSocket_.errorString().toStdString() << ")" << std::endl;
        } else {
            std::cout << "[PacketClient] command socket bound!" << std::endl;
        }
        // Create a 1MB buffer (only QTcpSocket)
        // Note, this option is ignored on QUdpSocket, therefore, the buffer size
        // is provided by the operating system.
        commandSocket_.setReadBufferSize(100000);
        //std::cout << "[PacketClient] ReceiveBuffer size = "
        //          << commandSocket.readBufferSize() << std::endl;
    } else {
        std::cout << "[PacketClient] Command socket already connected!" << std::endl;
    }


    // TODO: Be careful with the bind address of the dataSocket_, if bound to
    // multicastAddress_ or QHostAddress::AnyIPv4
    // Both approaches work, but multicastAddress_ maight not work for loopback,
    // which means that the server is on the same machine.
    // myAddress_ doens't work here when used in Multicast
    if(dataSocket_.state() == QUdpSocket::UnconnectedState) {
        dataSocket_.bind(multicastAddress_, dataPort_, QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
        if(dataSocket_.state() != QUdpSocket::BoundState) {
            std::cout << "[PacketClient] data socket bind failed! (error: "
                      << dataSocket_.errorString().toStdString() << ")" << std::endl;
        } else {
            std::cout << "[PacketClient] data socket bound!" << std::endl;
        }

        //join multicast group
        bool retValue = 0;
        retValue = dataSocket_.joinMulticastGroup(multicastAddress_, iFaceFromIP(myAddress_));
        if(!retValue){
            std::cout << "[PacketClient] join failed (error: "
                      << dataSocket_.errorString().toStdString() << ")" << std::endl;
        }
        // Create a 1MB buffer (only QTcpSocket)
        // Note, this option is ignored on QUdpSocket, therefore, the buffer size
        // is provided by the operating system.
        dataSocket_.setReadBufferSize(100000);
        //std::cout << "[PacketClient] ReceiveBuffer size = "
        //          << commandSocket.readBufferSize() << std::endl;
    } else {
        std::cout << "[PacketClient] Data socket already connected!" << std::endl;
    }

}

void NatNet::initCommunication()
{
    // send initial ping command
    sPacket packetOut;
    packetOut.iMessage = NAT_PING;
    packetOut.nDataBytes = 0;
    int nTries = 3;
    while (nTries--)
    {

        int iRet = commandSocket_.writeDatagram((char *)&packetOut, 4 + packetOut.nDataBytes, serverAddress_, commandPort_);
        if(iRet < 0)
            break;
    }

    std::cout << "Packet Client started" << std::endl << std::endl;
}

QNetworkInterface NatNet::iFaceFromIP(QHostAddress address)
{
    foreach(QNetworkInterface interface, QNetworkInterface::allInterfaces()){
        foreach(QNetworkAddressEntry addressEntry, interface.addressEntries()){
            if(addressEntry.ip().protocol() == QAbstractSocket::IPv4Protocol){
                if(addressEntry.ip() == address)
                    return interface;
            }
        }
    }
    QNetworkInterface empty;
    return empty;
}

void NatNet::autoConnectToServer()
{
    // Wait for datagram on waitForServer slot
    QObject::connect(&dataSocket_, SIGNAL(readyRead()),
            this, SLOT(waitForServer()));

    std::cout << "Attempting autoconnect to server." << std::endl;
    createSockets();
}

void NatNet::populateBuggyServers()
{
    buggyServer buggyServer1;   // Fix issue #5
    buggyServer1.name = "Motive";
    buggyServer1.version[0] = 1;
    buggyServer1.version[1] = 73;
    buggyServer1.version[2] = 0;
    buggyServer1.version[3] = 0;
    fixBuggyServer_[1] = false;

    buggyServers_.push_front(buggyServer1);
}

void NatNet::checkoForBuggyServers()
{
    int serverNumber = 0;
    for(std::list<buggyServer>::iterator listIt = buggyServers_.begin();
        listIt != buggyServers_.end(); listIt++, serverNumber++){
        bool buggy = false;
        if(listIt->name.compare(serverName_) == 0){ // If names are equal
            for(int i=0; i<4; i++){
                if(ServerVersion_[i] != listIt->version[i]){    // If versions are different
                    // Server not buggy
                    buggy = false;
                    break;
                } else {
                    // Server may be buggy
                    buggy = true;
                    continue;
                }
            }
            if(buggy){
                std::cout << "[Warning]" <<"Buggy server!" << std::endl;
                fixBuggyServer_[serverNumber+1] = true;
            }
        }
    }
}

void NatNet::sendCommand(NatNet::Command command)
{
    sPacket packetOut;
    std::auto_ptr<std::string> pRequest;
    switch (command){
    case DATA:
        // send NAT_REQUEST_MODELDEF command to server (will respond on the "commandReading" slot)
        packetOut.iMessage = NAT_REQUEST_MODELDEF;
        packetOut.nDataBytes = 0;
        std::cout << "Sending NAT_REQUEST_MODELDEF" << std::endl;
       break;
    case FRAME:
        // send NAT_REQUEST_FRAMEOFDATA (will respond on the "commandReading" slot)
        packetOut.iMessage = NAT_REQUEST_FRAMEOFDATA;
        packetOut.nDataBytes = 0;
        std::cout << "Sending NAT_REQUEST_FRAMEDATA" << std::endl;
        break;
    case TEST:
        // send NAT_MESSAGESTRING (will respond on the "commandReading" slot)
        pRequest = std::auto_ptr<std::string> (new std::string("TestRequest"));
        packetOut.iMessage = NAT_REQUEST;
        packetOut.nDataBytes = pRequest->length() + 1;
        strcpy(packetOut.Data.szData, pRequest->c_str());
        std::cout << "Sending NAT_REQUEST" << std::endl;
        break;
    case PING:
        // send NAT_MESSAGESTRING (will respond on the "commandReading" slot)
        pRequest = std::auto_ptr<std::string> (new std::string("Ping"));
        packetOut.iMessage = NAT_PING;
        packetOut.nDataBytes = pRequest->length() + 1;
        strcpy(packetOut.Data.szData, pRequest->c_str());
        std::cout << "Sending NAT_PING" << std::endl;
        break;
    default:
        break;
    }

    int nTries = 3;
    int iRet = 0;
    while (nTries--){
        iRet = commandSocket_.writeDatagram((char *)&packetOut, 4 + packetOut.nDataBytes, serverAddress_, commandPort_);
        if(iRet < 0)
            break;
    }

}

void NatNet::unpack(char *pData)
{
    int major = NatNetVersion_[0];
    int minor = NatNetVersion_[1];

    char *ptr = pData;

    std::cout << "Begin Packet" << std::endl << "-------" << std::endl;

    // message ID
    int MessageID = 0;
    std::memcpy(&MessageID, ptr, 2); ptr += 2;
    std::cout << "Message ID : " << MessageID << std::endl;

    // size
    int nBytes = 0;
    std::memcpy(&nBytes, ptr, 2); ptr += 2;
    std::cout << "Byte count : " << nBytes << std::endl;

    if(MessageID == 7)      // FRAME OF MOCAP DATA packet
    {
        // frame number
        int frameNumber = 0; std::memcpy(&frameNumber, ptr, 4); ptr += 4;
        std::cout << "Frame # : " << frameNumber << std::endl;

        // number of data sets (markersets, rigidbodies, etc)
        int nMarkerSets = 0; std::memcpy(&nMarkerSets, ptr, 4); ptr += 4;
        std::cout << "Marker Set Count : " << nMarkerSets << std::endl;

        for (int i=0; i < nMarkerSets; i++)
        {
            // Markerset name
            std::string szName(ptr);
            int nDataBytes = (int) szName.length() + 1;
            ptr += nDataBytes;
            std::cout << "Model Name: " << szName << std::endl;

            // marker data
            int nMarkers = 0; std::memcpy(&nMarkers, ptr, 4); ptr += 4;
            std::cout << "Marker Count : " << nMarkers << std::endl;

            for(int j=0; j < nMarkers; j++)
            {
                float x = 0; std::memcpy(&x, ptr, 4); ptr += 4;
                float y = 0; std::memcpy(&y, ptr, 4); ptr += 4;
                float z = 0; std::memcpy(&z, ptr, 4); ptr += 4;
                std::cout << "\tMarker " << j << " : [x=" << x << ",y=" <<
                             y << ",z=" << z << "]" << std::endl;
            }
        }

        // unidentified markers
        int nOtherMarkers = 0; std::memcpy(&nOtherMarkers, ptr, 4); ptr += 4;
        std::cout << "Unidentified Marker Count : " << nOtherMarkers << std::endl;
        for(int j=0; j < nOtherMarkers; j++)
        {
            float x = 0.0f; memcpy(&x, ptr, 4); ptr += 4;
            float y = 0.0f; memcpy(&y, ptr, 4); ptr += 4;
            float z = 0.0f; memcpy(&z, ptr, 4); ptr += 4;
            std::cout << "\tMarker " << j << " : pos = [" << x << "," <<
                         y << "," << z << "]" << std::endl;
        }

        // rigid bodies
        int nRigidBodies = 0;
        std::memcpy(&nRigidBodies, ptr, 4); ptr += 4;
        std::cout << "Rigid Body Count : " << nRigidBodies << std::endl;
        for (int j=0; j < nRigidBodies; j++) // For every rogid body
        {
            // rigid body pos/ori

            // Issue #5 Fix: Just read a short int (2 bytes of data) from
            // the data packet for the ID.
            // NOTE: The other 2 bytes seem to be the Skeleton ID.
            // Move the data pointer 4 bytes in order to jump to the
            // rigid body data.
            int ID = 0; std::memcpy(&ID, ptr, 2); ptr += 4; // Issue #5 Fix.
            float x = 0.0f; std::memcpy(&x, ptr, 4); ptr += 4;
            float y = 0.0f; std::memcpy(&y, ptr, 4); ptr += 4;
            float z = 0.0f; std::memcpy(&z, ptr, 4); ptr += 4;
            float qx = 0; std::memcpy(&qx, ptr, 4); ptr += 4;
            float qy = 0; std::memcpy(&qy, ptr, 4); ptr += 4;
            float qz = 0; std::memcpy(&qz, ptr, 4); ptr += 4;
            float qw = 0; std::memcpy(&qw, ptr, 4); ptr += 4;
            std::cout << "ID : " << ID << std::endl;
            std::cout << "pos: [" << x << "," << y << "," << z << "]" << std::endl;
            std::cout << "ori: [" << qx << "," << qy << "," << qz << "," << qw << "]" << std::endl;

            // associated marker positions
            int nRigidMarkers = 0;  std::memcpy(&nRigidMarkers, ptr, 4); ptr += 4;
            std::cout << "Marker Count: " << nRigidMarkers << std::endl;
            int nBytes = nRigidMarkers*3*sizeof(float);
            float* markerData = new float[nBytes];
            std::memcpy(markerData, ptr, nBytes);
            ptr += nBytes;

            if(major >= 2) // New data format
            {
                // associated marker IDs
                nBytes = nRigidMarkers*sizeof(int);
                int* markerIDs = new int[nBytes];
                std::memcpy(markerIDs, ptr, nBytes);
                ptr += nBytes;

                // associated marker sizes
                nBytes = nRigidMarkers*sizeof(float);
                float* markerSizes = new float[nBytes];
                std::memcpy(markerSizes, ptr, nBytes);
                ptr += nBytes;

                for(int k=0; k < nRigidMarkers; k++)
                {
                    std::cout << "\tMarker " << k << ": id=" << markerIDs[k] <<
                                 "\tsize=" << markerSizes[k] << "\tpos=[" <<
                                 markerData[k*3] << "," << markerData[k*3+1] <<
                                 "," << markerData[k*3+2] << "]" << std::endl;
                }

                // release resources
                if(markerIDs)
                    delete[] markerIDs;
                if(markerSizes)
                    delete[] markerSizes;

            }
            else    // Old data format
            {
                for(int k=0; k < nRigidMarkers; k++)
                {
                    std::cout << "\tMarker " << k << ": pos = [" <<
                                 markerData[k*3] << "," << markerData[k*3+1] <<
                                 "," << markerData[k*3+2] << "]" << std::endl;
                }
            }
            if(markerData)
                delete[] markerData;

            if(major >= 2)
            {
                // Mean marker error
                float fError = 0.0f; std::memcpy(&fError, ptr, 4); ptr += 4;
                std::cout << "Mean marker error: " << fError << std::endl;
            }

            // 2.6 and later
            if( ((major == 2)&&(minor >= 6)) || (major > 2) || (major == 0) )
            {
                // params
                short params = 0; std::memcpy(&params, ptr, 2); ptr += 2;
                bool bTrackingValid = params & 0x01; // 0x01 : rigid body was successfully tracked in this frame
            }

        } // next rigid body


        // skeletons (version 2.1 and later)
        if( ((major == 2)&&(minor>0)) || (major>2))
        {
            int nSkeletons = 0;
            std::memcpy(&nSkeletons, ptr, 4); ptr += 4;
            std::cout << "Skeleton Count : " << nSkeletons << std::endl;
            for (int j=0; j < nSkeletons; j++)
            {
                // skeleton id
                int skeletonID = 0;
                std::memcpy(&skeletonID, ptr, 4); ptr += 4;
                std::cout << "Skeleton ID: " << skeletonID << std::endl;
                // # of rigid bodies (bones) in skeleton
                int nRigidBodies = 0;
                std::memcpy(&nRigidBodies, ptr, 4); ptr += 4;
                std::cout << "Rigid Body Count : " << nRigidBodies << std::endl;
                for (int j=0; j < nRigidBodies; j++)
                {
                    // rigid body pos/ori

                    // Issue #5 Fix: Just read a short int (2 bytes of data) from
                    // the data packet for the ID.
                    // NOTE: The other 2 bytes seem to be the Skeleton ID.
                    // Move the data pointer 4 bytes in order to jump to the
                    // rigid body data.
                    int ID = 0; std::memcpy(&ID, ptr, 2); ptr += 4; // Issue #5 Fix.
                    float x = 0.0f; std::memcpy(&x, ptr, 4); ptr += 4;
                    float y = 0.0f; std::memcpy(&y, ptr, 4); ptr += 4;
                    float z = 0.0f; std::memcpy(&z, ptr, 4); ptr += 4;
                    float qx = 0; std::memcpy(&qx, ptr, 4); ptr += 4;
                    float qy = 0; std::memcpy(&qy, ptr, 4); ptr += 4;
                    float qz = 0; std::memcpy(&qz, ptr, 4); ptr += 4;
                    float qw = 0; std::memcpy(&qw, ptr, 4); ptr += 4;
                    std::cout << "ID : " << ID << std::endl;
                    std::cout << "pos: [" << x << "," << y << "," << z << "]" << std::endl;
                    std::cout << "ori: [" << qx << "," << qy << "," << qz << "," << qw << "]" << std::endl;

                    // associated marker positions
                    int nRigidMarkers = 0;  std::memcpy(&nRigidMarkers, ptr, 4); ptr += 4;
                    std::cout << "Marker Count: " << nRigidMarkers << std::endl;
                    int nBytes = nRigidMarkers*3*sizeof(float);
                    float* markerData = new float[nBytes];
                    std::memcpy(markerData, ptr, nBytes);
                    ptr += nBytes;

                    // associated marker IDs
                    nBytes = nRigidMarkers*sizeof(int);
                    int* markerIDs = new int[nBytes];
                    std::memcpy(markerIDs, ptr, nBytes);
                    ptr += nBytes;

                    // associated marker sizes
                    nBytes = nRigidMarkers*sizeof(float);
                    float* markerSizes = new float[nBytes];
                    std::memcpy(markerSizes, ptr, nBytes);
                    ptr += nBytes;

                    for(int k=0; k < nRigidMarkers; k++)
                    {
                        std::cout << "\tMarker " << k << ": id=" << markerIDs[k] <<
                                     "\tsize=" << markerSizes[k] << "\tpos=[" <<
                                     markerData[k*3] << "," << markerData[k*3+1] <<
                                     "," << markerData[k*3+2] << "]" << std::endl;
                    }

                    // Mean marker error
                    float fError = 0.0f; std::memcpy(&fError, ptr, 4); ptr += 4;
                    std::cout << "Mean marker error: " << fError << std::endl;

                    // release resources
                    if(markerIDs)
                        delete[] markerIDs;
                    if(markerSizes)
                        delete[] markerSizes;
                    if(markerData)
                        delete[] markerData;

                    // Issue #5 Fix : Motive:body is appending 2 bytes of zero data
                    // to the end of each rigid body (belonging to a skeleton) data.
                    // NOTE: NaturalPoint, Inc was notified about this issue,}
                    // and this was the solution that they proposes.
                    // Apparently, new tracking flags for the skeleton bone's rigid
                    // body were getting overlooked in the skeleton depacketization
                    // causing the alignment issue.

                    // Tracking flags (2.6 and later)
                    if( ((major == 2)&&(minor >= 6)) || (major > 2) || (major == 0) )
                    {
                          // params
                          short params = 0; std::memcpy(&params, ptr, 2); ptr += 2; // Issue #06 Fix.
                          bool bTrackingValid = params & 0x01;
                          // 0x01 : skeleton's rigid body was successfully tracked in this frame
                    }

                } // next rigid body

            } // next skeleton
        } // skeletons (version 2.1 and later)

        // labeled markers (version 2.3 and later)
        if( ((major == 2)&&(minor>=3)) || (major>2))
        {
            int nLabeledMarkers = 0;
            std::memcpy(&nLabeledMarkers, ptr, 4); ptr += 4;
            std::cout << "Labeled Marker Count : " << nLabeledMarkers << std::endl;
            for (int j=0; j < nLabeledMarkers; j++)
            {
                // id
                int ID = 0; std::memcpy(&ID, ptr, 4); ptr += 4;
                // x
                float x = 0.0f; std::memcpy(&x, ptr, 4); ptr += 4;
                // y
                float y = 0.0f; std::memcpy(&y, ptr, 4); ptr += 4;
                // z
                float z = 0.0f; std::memcpy(&z, ptr, 4); ptr += 4;
                // size
                float size = 0.0f; std::memcpy(&size, ptr, 4); ptr += 4;

                // 2.6 and later
                if( ((major == 2)&&(minor >= 6)) || (major > 2) || (major == 0) )
                {
                    // marker params
                    short params = 0; std::memcpy(&params, ptr, 2); ptr += 2;
                    bool bOccluded = params & 0x01;     // marker was not visible (occluded) in this frame
                    bool bPCSolved = params & 0x02;     // position provided by point cloud solve
                    bool bModelSolved = params & 0x04;  // position provided by model solve
                }

                std::cout << "ID  : " << ID << std::endl;
                std::cout << "pos : [" << x << "," << y << "," << z << "]" << std::endl;
                std::cout << "size: [" << size << "]" << std::endl;
            }
        } // labeled markers (version 2.3 and later)

        // timestamp and latency is the same
        // latency
        // Latency is the capture computer's hardware timestamp for the given frame,
        // which is also displayed in Motive in the Camera Preview viewport when
        // camera info is enabled.
        float badLatency = 0.0f; memcpy(&badLatency, ptr, 4);	ptr += 4;
        std::cout << "latency : " << badLatency << std::endl;

        // timecode
        unsigned int timecode = 0; 	std::memcpy(&timecode, ptr, 4);	ptr += 4;
        unsigned int timecodeSub = 0; std::memcpy(&timecodeSub, ptr, 4); ptr += 4;
        std::string strTimecode;

        if(timecodeStringify(timecode, timecodeSub, strTimecode)) {
            std::cout << "Timecode: " << strTimecode << std::endl;
        } else {
            std::cout << "Invalid SMPTE timecode!" << std::endl;
        }

        // timestamp
        double timestamp = 0.0f;
        // 2.7 and later - increased from single to double precision
        if( ((major == 2)&&(minor>=7)) || (major>2))
        {
            std::memcpy(&timestamp, ptr, 8); ptr += 8;
        }
        else
        {
            float fTemp = 0.0f;
            std::memcpy(&fTemp, ptr, 4); ptr += 4;
            timestamp = (double)fTemp;
        }

        std::cout << "timestamp: " << timestamp << std::endl;
        double latency = 0.0f;
        // Latency is zero the first time
        if(notFirstTimeLatency_) {
            latency = timestamp - lastTimestamp_;
        } else {
            notFirstTimeLatency_ = true;
        }
        lastTimestamp_ = timestamp;
        std::cout << "Latency: " << latency*1000 << " ms" << std::endl;

        // frame params
        short params = 0;  std::memcpy(&params, ptr, 2); ptr += 2;
        bool bIsRecording = params & 0x01;                  // 0x01 Motive is recording
        bool bTrackedModelsChanged = params & 0x02;         // 0x02 Actively tracked model list has changed


        // end of data tag
        int eod = 0; std::memcpy(&eod, ptr, 4); ptr += 4;
        if(eod != 0){
            std::cout << "Frame of MOCAP data packet corrupted!" << std::endl;
        }
        std::cout << "End Packet" << std::endl << "-------------" << std::endl;
    } // FRAME OF MOCAP DATA packet
    else if(MessageID == 5) // Data Descriptions
    {
        // number of datasets
        int nDatasets = 0; std::memcpy(&nDatasets, ptr, 4); ptr += 4;
        std::cout << "Dataset Count : " << nDatasets << std::endl;

        for(int i=0; i < nDatasets; i++)
        {
            std::cout << "Dataset " << i << std::endl;
            int type = 0; std::memcpy(&type, ptr, 4); ptr += 4;
            std::cout << "Type : " << type; // Formating continues on type specific code


            if(type == 0)   // markerset
            {
                std::cout << " (Markerset)" << std::endl;
                // name
                std::string szName(ptr);
                int nDataBytes = szName.length() + 1;
                ptr += nDataBytes;
                std::cout << "Markerset Name: " << szName << std::endl;

                // marker data
                int nMarkers = 0; std::memcpy(&nMarkers, ptr, 4); ptr += 4;
                std::cout << "Marker Count : " << nMarkers << std::endl;

                for(int j=0; j < nMarkers; j++)
                {
                    std::string szName(ptr);
                    int nDataBytes = szName.length() + 1;
                    ptr += nDataBytes;
                    std::cout << "Marker Name: " << szName << std::endl;
                }
            } // markerset
            else if(type ==1)   // rigid body
            {
                std::cout << " (Rigid body)" << std::endl;

                if(major >= 2)
                {
                    // name
                    std::string szName(ptr);
                    ptr += szName.length() + 1;
                    std::cout << "Rigid Body Name: " << szName << std::endl;
                }

                int ID = 0; std::memcpy(&ID, ptr, 4); ptr +=4;
                std::cout << "Rigid Body ID : " << ID << std::endl;

                int parentID = 0; std::memcpy(&parentID, ptr, 4); ptr +=4;
                std::cout << "Parent ID : " << parentID << std::endl;

                float xoffset = 0; std::memcpy(&xoffset, ptr, 4); ptr +=4;
                std::cout << "X Offset : " << xoffset << std::endl;

                float yoffset = 0; std::memcpy(&yoffset, ptr, 4); ptr +=4;
                std::cout << "Y Offset : " << yoffset << std::endl;

                float zoffset = 0; std::memcpy(&zoffset, ptr, 4); ptr +=4;
                std::cout << "Z Offset : " << zoffset << std::endl;
            } // rigid body
            else if(type ==2)   // skeleton
            {
                std::cout << " (Skeleton)" << std::endl;

                std::string szName(ptr);
                ptr += szName.length() + 1;
                std::cout << "Skeleton Name: " << szName << std::endl;

                int ID = 0; std::memcpy(&ID, ptr, 4); ptr +=4;
                std::cout << "Skeleton ID : " << ID << std::endl;

                int nRigidBodies = 0; std::memcpy(&nRigidBodies, ptr, 4); ptr +=4;
                std::cout << "RigidBody (Bone) Count : " << nRigidBodies << std::endl;

                for(int i=0; i< nRigidBodies; i++)
                {
                    if(major >= 2)
                    {
                        // RB name
                        std::string szName(ptr);
                        ptr += szName.length() + 1;
                        std::cout << "Rigid Body Name: " << szName << std::endl;
                    }

                    int ID = 0; std::memcpy(&ID, ptr, 4); ptr +=4;
                    std::cout << "RigidBody ID : " << ID << std::endl;

                    int parentID = 0; std::memcpy(&parentID, ptr, 4); ptr +=4;
                    std::cout << "Parent ID : " << parentID << std::endl;

                    float xoffset = 0; std::memcpy(&xoffset, ptr, 4); ptr +=4;
                    std::cout << "X Offset : " << xoffset << std::endl;

                    float yoffset = 0; std::memcpy(&yoffset, ptr, 4); ptr +=4;
                    std::cout << "Y Offset : " << yoffset << std::endl;

                    float zoffset = 0; std::memcpy(&zoffset, ptr, 4); ptr +=4;
                    std::cout << "Z Offset : " << zoffset << std::endl;
                }
            } // skeleton

        }   // next dataset

        // Note: The Data Descriptions packet doesn't end with an EOD (end of data tag),
        // it just ends after the last byte of data is delivered.
        std::cout << "End Packet" << std::endl << "-------------" << std::endl;
    } // Data Descriptions
    else
    {
        std::cout << "Unrecognized Packet Type." << std::endl;
    }

}

void NatNet::setVersions(NatNet::sPacket &PacketIn)
{
//    // This can be used in conjunction with precompiler directive:
//    // #pragma pack(push, 1)
//    // See .h file for more info.
//    for(int i=0; i<4; i++){
//        NatNetVersion_[i] = (int)PacketIn.Data.Sender.NatNetVersion[i];
//        ServerVersion_[i] = (int)PacketIn.Data.Sender.Version[i];
//    }

    // This is the alternative to accessind directly the sPacket structure fields
    // The pointer needs to traverse the datagram manually.
    char *ptr = (char*)&PacketIn;
    // message ID
    ptr += 2;
    // payload size
    ptr += 2;
    // sending app's name
    serverName_ = std::string(ptr);
    std::cout << "Server app name:" << serverName_ << std::endl;
    ptr += MAX_NAMELENGTH;
    // sending app's version [major.minor.build.revision]
    for(int i=0; i<4; i++){
        ServerVersion_[i] = (int)*ptr; ptr++;
    }
    // sending app's NatNet version [major.minor.build.revision]
    for(int i=0; i<4; i++){
        NatNetVersion_[i] = (int)*ptr; ptr++;
    }

    std::cout << "Server version: " << ServerVersion_[0] << "." << ServerVersion_[1]
              << "." << ServerVersion_[2] << "." << ServerVersion_[3] << std::endl;
    std::cout << "NatNet version: " << NatNetVersion_[0] << "." << NatNetVersion_[1]
              << "." << NatNetVersion_[2] << "." << NatNetVersion_[3] << std::endl;


    if(state_ == CONNECTING) {
        checkoForBuggyServers();
        // Read dataSocket data (multicast streaming) in the dataReading() slot
        // just after the NatNet version has been established
        QObject::connect(&dataSocket_, SIGNAL(readyRead()),
            this, SLOT(dataReading()));

        // Note: An incoming datagram should be read when you receive the
        // readyRead() signal, otherwise this signal will not be emitted for the
        // next datagram.
        // Read datagram in order to emit the readyRead signal again when a datagram
        // is available.
        if(dataSocket_.hasPendingDatagrams()){
            dataReading();
        }

        state_ = CONNECTED;
    }

}

