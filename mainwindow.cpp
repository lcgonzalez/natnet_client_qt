// Copyright (C) 2015 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file mainwindow.cpp
//  @author Luis Gonzalez <lc.gonzalez23@gmail.com>
//  @version 1.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of NatNet Packet Client.
//
//                          License Agreement
//                     For the NatNet Packet Client
//
//    NatNet Packet Client is a program used to directly communicate to
//    a NatNet server, without using NatNet's API.
//    Copyright (C) 2015 Luis Carlos Gonzalez Garcia
//
//    NatNet Packet Client is free software: you can redistribute it and/or
//    modify it under the terms of the GNU General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    NatNet Packet Client is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "q_debugstream.h"

#include <iostream>
#include <memory>
#include <QNetworkInterface>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // The stretch factor is used to make the tab (widget at index 0) retain its
    // current size when maximizing the window, while the console widget will
    // grow
    // This property connot be currently set using Qt Creator 2.6.1
    ui->splitter->setStretchFactor(0,0);
    ui->splitter->setStretchFactor(1,1);
    Q_DebugStream_ = std::auto_ptr<Q_DebugStream>(new Q_DebugStream(std::cout, ui->consoleOutput));

    populate_network_IFace();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectButton_clicked()
{
    std::cout << "Server address: " << ui->serverAddress->text().toStdString() << std::endl;
    std::cout << "Client address: " << ui->selected_IFace->currentText().toStdString() << std::endl;

    Natnet.start(ui->serverAddress->text(), ui->selected_IFace->currentText());
}

void MainWindow::on_sendCommandButton_clicked()
{
    std::cout << "Command to send: " <<ui->commandToSend->currentIndex() << std::endl;
    NatNet::Command command;
    switch (ui->commandToSend->currentIndex()){
    case 0:
        command = NatNet::DATA;
        break;
    case 1:
        command = NatNet::FRAME;
        break;
    case 2:
        command = NatNet::TEST;
        break;
    case 3:
        command = NatNet::PING;
        break;
    default:
        break;
    }

    Natnet.sendCommand(command);
}

void MainWindow::populate_network_IFace()
{
    foreach(QHostAddress address, QNetworkInterface::allAddresses()){
        if(address.protocol() == QAbstractSocket::IPv4Protocol)
            ui->selected_IFace->addItem(address.toString());
    }
}

void MainWindow::on_disconnectButton_clicked()
{
    Natnet.disconnect();
}
