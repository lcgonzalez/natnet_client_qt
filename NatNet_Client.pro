#-------------------------------------------------
#
# Project created by QtCreator 2015-03-19T16:16:30
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NatNet_Client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    natnet.cpp \
    q_debugstream.cpp

HEADERS  += mainwindow.h \
    natnet.h \
    q_debugstream.h

FORMS    += mainwindow.ui
